import React from 'react';
import ReactDOM from 'react-dom';
import { Image, Jumbotron } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { Container, Row, Col} from 'react-grid-system';

class GetHeroes extends React.Component{
    state = {
        heroes: []
    }
    
    constructor(){
        super();
    }

    componentWillMount(){
        fetch('http://gateway.marvel.com/v1/public/characters?ts=1&apikey=298bab46381a6daaaee19aa5c8cafea5&hash=7f61efcc06d409d98e97c9dc65e47024&offset=0')
        .then((response) => {
            response.json().then((data) => {
                this.setState({heroes: data.data.results});
            });
        });
    }

    render() {
        return (
            <div>
                <Container>
                    <Jumbotron className="heroList">
                         <h1 className="title">Liste des super héros</h1>
                    </Jumbotron>
                    <Row>
                        {this.state.heroes.map((hero) => (
                            <Col sm={4} key={hero.id}>
                                    <Image src={`${hero.thumbnail.path}.${hero.thumbnail.extension}`} responsive={true} className="image"/>
                                    <div className="heroName">
                                        <Link className="text" to={`/detail/${hero.id}`} key={hero.id} className="heroUrls">{hero.name}</Link>
                                    </div>
                                    <div className="link">
                                        {hero.urls && hero.urls.map((link) => (<a href={link.url} key={link.type} className="heroUrls">{link.type}</a>))}                                        
                                    </div>
                            </Col>
                        ))}
                    </Row>
                </Container>
            </div>
        );
    }
}

export default GetHeroes;

