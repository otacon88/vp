import React from 'react';
import ReactDOM from 'react-dom';
import AppRouter from './routers/AppRouter.js';
import './styles/styles.scss';
import '!style-loader!css-loader!bootstrap/dist/css/bootstrap.css';

ReactDOM.render(<AppRouter />, document.getElementById('app'));