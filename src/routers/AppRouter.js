import React from 'react';
import { BrowserRouter, Route, Switch, Link, NavLink } from 'react-router-dom';
import GetHeroes from '../components/getHeroes.js';
import GetHero from '../components/getHero.js';

const NotFoundPage = () => (<div>404</div>);

const AppRouter = () => (
    <BrowserRouter>
        <div>
            <Switch>
                <Route path="/" component={GetHeroes} exact={true} />
                    <Route path="/detail/:id" component={GetHero} exact={true} />
                <Route component={NotFoundPage} />
            </Switch>
        </div>
    </BrowserRouter>
);

export default AppRouter;