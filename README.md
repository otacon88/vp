# vp

##Required :

- nodejs
- npm
- yarn

To start the app you have to install all dependencies with yarn in command line :
`yarn install`

To launch the app use the following command in your terminal :
`yarn run dev-server`

Open the url `http://localhost:8080/` in your browser 