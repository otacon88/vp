import React from 'react';
import ReactDOM from 'react-dom';
import { NavLink } from 'react-router-dom';
import { Image, ListGroup, ListGroupItem, Jumbotron } from 'react-bootstrap';
import { Container, Row, Col} from 'react-grid-system';

class GetHeroe extends React.Component{
    state = {
        img: {},
        collection: []
    }
    
    constructor(){
        super();
    }

    componentWillMount(){
        fetch(`http://gateway.marvel.com/v1/public/characters/${this.props.match.params.id}?ts=1&apikey=298bab46381a6daaaee19aa5c8cafea5&hash=7f61efcc06d409d98e97c9dc65e47024`)
        .then((response) => {
            response.json().then((data) => {
                this.setState({  
                    collection: data.data.results
                });
            });
        });
    }

    render() {
        return (
            <div>
               {    this.state.collection && this.state.collection.map((hero) => {
                        return (
                            <Container key={hero.id}>
                                    <Jumbotron>
                                            <h1 className="title">Fiche d'identité : {hero.name}</h1>
                                            <div className="heroResume">
                                                {hero.description && (<div key={hero.id}>{hero.description}</div>)}
                                            </div>
                                            <NavLink  className="navlink" to="/">Retour à la liste des super héros</NavLink>
                                    </Jumbotron>
                                <Row>
                                    
                                    <Col xs={6} md={4}>
                                            <Image src={`${hero.thumbnail.path}.${hero.thumbnail.extension}`}/>
                                    </Col>

                                    <Col xs={6} md={4}>
                                        <div>
                                        {   hero.comics 
                                            && hero.comics.items 
                                            && hero.comics.items.length > 0 
                                            && (<h1>Comics</h1>)
                                        }

                                        {hero.comics.items.map((item) => 
                                            (
                                                <ListGroup key={item.name}>
                                                    <ListGroupItem>
                                                        {item.name}
                                                    </ListGroupItem>
                                                </ListGroup>
                                            )
                                        )}
                                        </div>
                                    </Col>

                                    <Col xs={6} md={4}>
                                        <div>
                                        {   hero.series 
                                            && hero.series.items 
                                            && hero.series.items.length > 0 
                                            && (<h1>Series</h1>)
                                        }

                                        { hero.series.items.map((item) => 
                                            (<ListGroup key={item.name}>
                                                <ListGroupItem>
                                                   {item.name}
                                                </ListGroupItem>
                                            </ListGroup>)   
                                        )}
                                        </div>           
                                    </Col>

                                </Row>
                            </Container>);
                   })
                }
               
            </div>
        );
    }
}

export default GetHeroe;